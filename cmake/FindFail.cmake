#
#   Urggr - An horizontal scrolling shoot'em up.
#   Copyright 2008 Antoine Chavasse <a.chavasse@gmail.com>
# 
#   This file is part of Urggr.
#
#   Urggr is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License version 3
#   as published by the Free Software Foundation.
#
#   Urggr is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# For now we only look for the lua modules. In the future I need to make
# fail install its headers and libraries and detect those too, but it
# won't be needed until I need to implement C++ classes in Urggr.
set( FAIL_FOUND "NO" )

find_path( FAIL_LUA_MODULES_DIR
	fail.so

	/usr/lib/lua/5.1
	/usr/local/lib/lua/5.1
)

if( FAIL_LUA_MODULES_DIR )
	set( FAIL_FOUND "YES" )
endif( FAIL_LUA_MODULES_DIR )

if( FAIL_FOUND )
	if( NOT FAIL_FIND_QUIETLY )
		message( STATUS "Found Fail: ${FAIL_LUA_MODULES_DIR}" )
	endif( NOT FAIL_FIND_QUIETLY )
else( FAIL_FOUND )
	if( FAIL_FIND_REQUIRED )
		message( FATAL_ERROR "Could not find Fail" )
	endif( FAIL_FIND_REQUIRED )
endif( FAIL_FOUND )
