--[[
    Urggr - An horizontal scrolling shoot'em up.
    Copyright 2008 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Urggr.

    Urggr is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Urggr is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

require 'fail.utils'
require 'fail.math'
sg = require 'fail.scenegraph'
col = require 'fail.bullet.collision'

Level = fail.utils.class
{
 	function( self )
		self.gameobjects = {}

		-- The top level container of the rendering scene graph.
		-- Just a group because there's nothing else currently,
		-- and this project won't be needing anything more sophisticated
		-- for a while.
		self.renderablegroup = sg.Group()
		
		-- Create the bullet collision world
		local broadphase = col.AxisSweep3(
			fail.math.Vector3f( -1000, -1000, -1000 ),
			fail.math.Vector3f( 1000, 1000, 1000 ) )
		self.btcollisionworld = col.World( broadphase )

		self:addGameObject( urggr.Camera() )
		self:addGameObject( urggr.PlayerShip( self ) )
		self:addGameObject( urggr.TestObstacle( self ) )
	end
}

function Level:addGameObject( gobj )
	table.insert( self.gameobjects, gobj )
end

function Level:addRenderable( renderable )
	self.renderablegroup:add( renderable )
end

function Level:addCollisionObject( colobj )
	self.btcollisionworld:addObject( colobj )
end

function Level:renderingUpdate( deltatime )

	self.btcollisionworld:performDiscreteCollisionDetection()

	for i,v in ipairs( self.gameobjects ) do
		v:renderingUpdate( deltatime )
	end
end

function Level:evaluateRender( renderpass )
	self.renderablegroup:evaluate( renderpass )
end
