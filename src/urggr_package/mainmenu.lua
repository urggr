--[[
    Urggr - An horizontal scrolling shoot'em up.
    Copyright 2008 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Urggr.

    Urggr is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Urggr is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

require 'qtcore'
require 'qtgui'
require 'fail.utils'
require 'fail.math'
sg = require 'fail.scenegraph'

MainMenu = fail.utils.class
{		
 	function( self, view )
		-- Create camera
		self.camframe = sg.Frame()
		self.camera = sg.Camera( camframe )
		--self.camframe.LocalToParent.position.y = -7
		--camframe:dirty()
			
		-- Create viewports
		self.viewport = sg.ViewPort( sg.PerspectiveProjection(), self.camera )
		self.viewport.Rect = fail.math.Rectu32( 0, 0, 1024, 768 )
		
		self.mainpass = sg.RenderPass( self.viewport )
		self.mainpass.bClearDepthBuffer = true
		self.mainpass.bClearColorBuffer = true
		
		self.qtscene = QGraphicsScene()
		
		-- Note: storing the scene in a global variable is necessary to work around a problem with lqt:
		-- Qt objects wrapped by Qt keep only a weak reference to the lua table where virtual function overloads
		-- are stored. So if a garbage collection happens when outside of a lua virtual function overload, there's no
		-- hard reference to the lua overload and it just goes poof,
		menuscene = self.qtscene
		
		self.qtscene.drawBackground = function() self:render() end
		
		-- The menu will only consist of a "start game" push button for now.
		self.startbutton = QPushButton( QString.new( "Start game" ) )
		self.qtscene:addWidget( self.startbutton )
		
		-- TODO: write some helpers to hide some of those incantations: conceptually I just want to bind a lua closure to a signal.
		-- it doesn't matter what object the slot I create for the closure is, so it may aswell be in some sort of global qobject
		-- created by the helper just for that purpose.		
		self.qtscene:__addmethod( "lulz", 'clicked()', function( self ) urggr.Game( view ) end )
		QObject.connect( self.startbutton, '2clicked()', self.qtscene, '1clicked()' )

		-- Setup a timer to update the scene periodically, even though there's nothing animated in there yet.
		-- It's to work around a problem where the widgets fail to appear at times, at least on my machine.
		self.lasttime = 0
		self.time = QTime()
		self.time:start()

		view:setScene( self.qtscene )
	end
}

function MainMenu:render()

	--collectgarbage()

	local deltatime = self.time:elapsed() - self.lasttime
	self.lasttime = self.lasttime + deltatime

	sg.RenderPass.ClearAll()
	self.mainpass:prependToRenderList()
	--scene:evaluate( scenepass )
	sg.RenderPass.RenderAll( true )
	
	--collectgarbage()

	QTimer.singleShot( 0, self.qtscene, "1update()" )

end
