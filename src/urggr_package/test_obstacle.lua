--[[
    Urggr - An horizontal scrolling shoot'em up.
    Copyright 2008-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Urggr.

    Urggr is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Urggr is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

require 'fail.utils'
require 'fail.math'
sg = require 'fail.scenegraph'
col = require 'fail.bullet.collision'

TestObstacle = fail.utils.class
{
	superclass = GameObject,

 	function( self, level )
		GameObject.init( self )

		self.frame = sg.Frame()
		self.frame.LocalToParent.position.x = 0 -- = fail.math.Vector3f( 0, 3, 3 )
		self.frame.LocalToParent.position.y = 3 -- = fail.math.Vector3f( 0, 3, 3 )
		self.frame.LocalToParent.position.z = 3 -- = fail.math.Vector3f( 0, 3, 3 )


		local mat = sg.Material()
		mat.Emission.value = fail.math.Vector4f( 0, 0, 1, 1 )
		mat.Specular.value = fail.math.Vector4f( 0, 0, 0, 1 )
		self.renderable = sg.shapes.Cube( mat, self.frame )
		
		level:addRenderable( self.renderable )

		self.CollisionObject = col.Object()
		self.CollisionObject.pShape = col.BoxShape( fail.math.Vector3f( 0.5, 0.5, 0.5 ) )
		self.CollisionObject.pFrame = self.frame
		level:addCollisionObject( self.CollisionObject )
		
		--self.sigcon = self.CollisionObject.Contact:connect( function( obj ) print "lol" end )
	end
}
