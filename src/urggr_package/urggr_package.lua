--[[
    Urggr - An horizontal scrolling shoot'em up.
    Copyright 2008 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Urggr.

    Urggr is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Urggr is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]
module( "urggr", package.seeall )

dofile '${LUA_PACKAGE_INSTALL_PATH}/mainmenu.lua'
dofile '${LUA_PACKAGE_INSTALL_PATH}/game.lua'
dofile '${LUA_PACKAGE_INSTALL_PATH}/level.lua'
dofile '${LUA_PACKAGE_INSTALL_PATH}/gameobject.lua'
dofile '${LUA_PACKAGE_INSTALL_PATH}/camera.lua'
dofile '${LUA_PACKAGE_INSTALL_PATH}/ship.lua'
dofile '${LUA_PACKAGE_INSTALL_PATH}/playership.lua'
dofile '${LUA_PACKAGE_INSTALL_PATH}/playerbullet.lua'
dofile '${LUA_PACKAGE_INSTALL_PATH}/test_obstacle.lua'
