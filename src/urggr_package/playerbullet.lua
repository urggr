--[[
    Urggr - An horizontal scrolling shoot'em up.
    Copyright 2008-2009 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Urggr.

    Urggr is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Urggr is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

require 'fail.utils'
require 'fail.math'
sg = require 'fail.scenegraph'
col = require 'fail.bullet.collision'

PlayerBullet = fail.utils.class
{
	superclass = GameObject,

 	function( self, level, startposition )
		GameObject.init( self )

		self.frame = sg.Frame()
		self.frame.LocalToParent.position.x = startposition.x
		self.frame.LocalToParent.position.y = startposition.y
		self.frame.LocalToParent.position.z = startposition.z


		local mat = sg.Material()
		mat.Emission.value = fail.math.Vector4f( 1, 0, 0, 1 )
		mat.Specular.value = fail.math.Vector4f( 0, 0, 0, 1 )
		self.renderable = sg.shapes.Cube( mat, self.frame )
		
		level:addRenderable( self.renderable )

		self.CollisionObject = col.Object()
		self.CollisionObject.pShape = col.BoxShape( fail.math.Vector3f( 0.5, 0.5, 0.5 ) )
		self.CollisionObject.pFrame = self.frame
		level:addCollisionObject( self.CollisionObject )
		
		--self.sigcon = self.CollisionObject.Contact:connect( function( obj ) print "lol" end )
	end
}

function PlayerBullet:renderingUpdate( deltatime )

	-- TODO: investigate why the thing crashes after a while if we don't explicitly
	-- call collect garbage regularly like so.
	-- Perhaps a garbage collection happening from an event handler or something results into a deadlock?
	-- 
	--collectgarbage()
--	print( deltatime )
--			self.camframe.LocalToParent.position.x = 7
	--	self.camframe:dirty()
--print(self.frame)
	
	--self.frame.LocalToParent.position.x = self.frame.LocalToParent.position.x + self.Speed.x * ( deltatime / 1000 )
	self.frame.LocalToParent.position.y = self.frame.LocalToParent.position.y + 10 * ( deltatime / 1000 )
	--self.frame.LocalToParent.position.z = self.frame.LocalToParent.position.z + self.Speed.z * ( deltatime / 1000 )
	self.frame:dirty()
end
