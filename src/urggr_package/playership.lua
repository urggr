--[[
    Urggr - An horizontal scrolling shoot'em up.
    Copyright 2008 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Urggr.

    Urggr is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Urggr is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

require 'fail.utils'
require 'fail.math'
sg = require 'fail.scenegraph'
require 'fail.scenegraph.shapes'
input = require 'fail.game.input'
col = require 'fail.bullet.collision'

PlayerShip = fail.utils.class
{
	superclass = Ship,

 	function( self, level )
		Ship.init( self )
		
		self.level = level
		self.frame = sg.Frame()

		-- Create a renderable for the player ship. For now we just conjure a procedural cone
		-- programmaticaly, at some point it'll be a renderable loaded from disk.
		local mat = sg.Material()
		mat.Emission.value = fail.math.Vector4f( 0, 1, 1, 1 )
		mat.Specular.value = fail.math.Vector4f( 0, 0, 0, 1 )
		self.renderable = sg.shapes.Cube( mat, self.frame )
	
	
	-- Serialization test. Load the player scenegraph from a file (previously saved in urggr.lua)
--		self.renderable = fail.fbffile.Reader.Load( "lulz.fbf" )
	--	self.frame = self.renderable.pFrame

		self.Speed = fail.math.Vector3f()

		level:addRenderable( self.renderable )
		
		-- Create a collision object for the player ship. A box for the time being since the other shape classes
		-- aren't wrapped in fail yet.
		-- TODO: also provide a way to set the collision filter mask.
		self.CollisionObject = col.Object()
		self.CollisionObject.pShape = col.BoxShape( fail.math.Vector3f( 0.5, 0.5, 0.5 ) )
		self.CollisionObject.pFrame = self.frame
		level:addCollisionObject( self.CollisionObject )
		
		input.InputManager.GetInstance().Actions.Left:bindHandler( function( pressed ) self:MoveLeft( pressed ) end )
		input.InputManager.GetInstance().Actions.Right:bindHandler( function( pressed ) self:MoveRight( pressed ) end )
		input.InputManager.GetInstance().Actions.Up:bindHandler( function( pressed ) self:MoveUp( pressed ) end )
		input.InputManager.GetInstance().Actions.Down:bindHandler( function( pressed ) self:MoveDown( pressed ) end )
		input.InputManager.GetInstance().Actions.Fire:bindHandler( function( pressed ) self:Fire( pressed ) end )
	end
}

function PlayerShip:MoveLeft( pressed )
	if( pressed ) then
		self.Speed.y = -5
	else
		self.Speed.y = 0
	end
end

function PlayerShip:MoveRight( pressed )
	if( pressed ) then
		self.Speed.y = 5
	else
		self.Speed.y = 0
	end
end

function PlayerShip:MoveUp( pressed )
	if( pressed ) then
		self.Speed.z = 5
	else
		self.Speed.z = 0
	end
end

function PlayerShip:MoveDown( pressed )
	if( pressed ) then
		self.Speed.z = -5
	else
		self.Speed.z = 0
	end
end


function PlayerShip:Fire( pressed )
	if( pressed ) then
		-- TODO: might be useful to provide some easier access to copy constructors to simply this.
		-- Perhaps in the form of a global fail.copy function?
		
		bulletpos = fail.math.Vector3f( --self.frame.LocalToParent.position )
			self.frame.LocalToParent.position.x,
			self.frame.LocalToParent.position.y,
			self.frame.LocalToParent.position.z
		)

		bulletpos.y = bulletpos.y + 0.7

		self.level:addGameObject( urggr.PlayerBullet( self.level, bulletpos ) )
	end
end

function PlayerShip:renderingUpdate( deltatime )

	-- 
	--collectgarbage()
--	print( deltatime )
--			self.camframe.LocalToParent.position.x = 7
	--	self.camframe:dirty()
--print(self.frame)
	
	self.frame.LocalToParent.position.x = self.frame.LocalToParent.position.x + self.Speed.x * ( deltatime / 1000 )
	self.frame.LocalToParent.position.y = self.frame.LocalToParent.position.y + self.Speed.y * ( deltatime / 1000 )
	self.frame.LocalToParent.position.z = self.frame.LocalToParent.position.z + self.Speed.z * ( deltatime / 1000 )
	self.frame:dirty()
end
