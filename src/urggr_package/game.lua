--[[
    Urggr - An horizontal scrolling shoot'em up.
    Copyright 2008 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Urggr.

    Urggr is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Urggr is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

require 'qtgui'
require 'fail.utils'
require 'fail.math'
sg = require 'fail.scenegraph'

Game = fail.utils.class
{
 	function( self, view )	
		-- Create camera

		-- TODO: that's just testing, this should be moved into the camera class.
		self.camframe = sg.Frame()
		self.camera = sg.Camera( self.camframe )
		--self.camera.FOV = 60
		local mtx = fail.math.Matrix44f()
		mtx:rotation( 90, fail.math.Vector3f( 0, 0, 1 ) )

		-- TODO: investigate why setting the position in mtx before assigning it
		-- to the localtoparent matrix doesn't work.
		--mtx.position.x = -7
		self.camframe.LocalToParent = mtx
		self.camframe.LocalToParent.position.x = 15
		self.camframe:dirty()
			
		-- Create viewport
		self.viewport = sg.ViewPort( sg.PerspectiveProjection(), self.camera )
		self.viewport.Rect = fail.math.Rectu32( 0, 0, 1024, 768 )
		
		self.mainpass = sg.RenderPass( self.viewport )
		self.mainpass.bClearDepthBuffer = true
		self.mainpass.bClearColorBuffer = true
		
		self.qtscene = QGraphicsScene()

		-- Note: storing the scene in a global variable is necessary to work around a problem with lqt:
		-- Qt objects wrapped by Qt keep only a weak reference to the lua table where virtual function overloads
		-- are stored. So if a garbage collection happens when outside of a lua virtual function overload, there's no
		-- hard reference to the lua overload and it just goes poof,
		gamescene = self.qtscene
		
		self.qtscene.drawBackground = function() self:render() end
		
		fail.game.input.Action( "Left", "Move left" )
		fail.game.input.Action( "Right", "Move right" )
		fail.game.input.Action( "Up", "Move up" )
		fail.game.input.Action( "Down", "Move down" )
		fail.game.input.Action( "Fire", "Fire primary weapon" )
		
		fail.game.input.InputManager.GetInstance():bindAction( "Left", "keyboard", "Left" )
		fail.game.input.InputManager.GetInstance():bindAction( "Right", "keyboard", "Right" )
		fail.game.input.InputManager.GetInstance():bindAction( "Up", "keyboard", "Up" )
		fail.game.input.InputManager.GetInstance():bindAction( "Down", "keyboard", "Down" )
		fail.game.input.InputManager.GetInstance():bindAction( "Fire", "keyboard", "Return" )
		fail.game.input.InputManager.GetInstance():bindAction( "Fire", "keyboard", "Space" )


		self.level = urggr.Level()
		
		self.lasttime = 0
		self.time = QTime()
		self.time:start()
		
		view:setScene( self.qtscene )
		
		fail.game.keyboard.Provider( self.qtscene )
	end
}

function Game:render()

	--collectgarbage()

	local deltatime = self.time:elapsed() - self.lasttime
	self.lasttime = self.lasttime + deltatime

	sg.RenderPass.ClearAll()
	self.mainpass:prependToRenderList()

	self.level:renderingUpdate( deltatime )
	self.level:evaluateRender( self.mainpass )
	sg.RenderPass.RenderAll( true )

	QTimer.singleShot( 0, self.qtscene, "1update()" )
	
end
