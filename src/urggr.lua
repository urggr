#!${LUA_INTERPRETER}
--[[
    Urggr - An horizontal scrolling shoot'em up.
    Copyright 2008 Antoine Chavasse <a.chavasse@gmail.com>
 
    This file is part of Urggr.

    Urggr is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    Urggr is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

package.path = package.path .. '${LUA_PACKAGE_PATH}'

Settings = {}

require 'qtcore'
require 'qtgui'
require 'qtopengl'
require 'fail'
require 'fail.math'
sg = require 'fail.scenegraph'
require 'fail.scenegraph.shapes'
require 'fail.bullet.collision'
require 'fail.game.keyboard'
require 'fail.gamestorage'
require 'fail.io'
require 'fail.fbffile'
require 'urggr'

qtapp = QApplication( 1 + select( '#', ... ), { arg[0], ...} )
qtapp.__gc = qtapp.delete

-- Serialization test. Create a cube with a material and save it to a file.
--[[local frame = sg.Frame()
local mat = sg.Material()
mat.Emission.value = fail.math.Vector4f( 0, 1, 1, 1 )
mat.Specular.value = fail.math.Vector4f( 0, 0, 0, 1 )
local renderable = sg.shapes.Cube( mat, frame )

fail.fbffile.Writer.Save( "lulz.fbf", renderable )]]


-- The current game scene (which can be the main menu, a game level, a mini game or basically anything occupying the rendering and UI)
-- will just provide a QGraphicsScene that we'll set as the current scene of the top level QGraphicsView. The actual fail engine action
-- shall be rendered with an overloaded drawBackground function, the QGraphicsItems should be used only for UI and hud elements.
-- The QGraphicsView used will always use a QGLWidget as its viewport.

-- Setup the main window, a QGraphicsView displaying our QGraphicsScene.
gv = QGraphicsView()

gv:setViewportUpdateMode( "FullViewportUpdate" )
gv:setCacheMode( {"CacheNone"} )
gv:setViewport( QGLWidget() )

-- Create the mainmenu. It'll set its own scene as the graphics view scene.
urggr.MainMenu( gv )

gv:show()
gv:resize(1024, 768);

-- TODO: separate the game logic updates and put them on a fixed time step.
-- Also change the input stuff to buffer the inputs and have them sent (and the
-- game event handler called) only when ticking the game updates (so the event handlers
-- can safely change the object states, which shouldn't be done in between game ticks)
--
-- It would be easier if it was possible to have qt process its events and render separately.
while( true ) do
	qtapp.processEvents()
	qtapp.sendPostedEvents()
end
